package nl.utwente.di.bookQuote;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets the Celsius and converts it to Fahrenheit
 */

public class BookQuote extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Calculator quoter;
	
    public void init() throws ServletException {
    	quoter = new Calculator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "The result of conversion from Celsius to Fahrenheit";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Temp. (Celsius): " +
                   request.getParameter("celsius") + "\n" +
                "  <P>Temp (Fahrenheit): " +
                   Double.toString(quoter.convertCelsius(Integer.parseInt(request.getParameter("celsius")))) +
                "</BODY></HTML>");
  }
  

}
